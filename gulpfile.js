var gulp = require('gulp');
var del = require('del');
var uglify = require('gulp-uglify');
var gfilter = require('gulp-filter');
var ngAnnotate = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var sourcemaps = require('gulp-sourcemaps');
var pump = require('pump');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var paths = {
    sass:       './app/scss/**/*.scss',
    js:         './app/js/**/*.js',
    templates:  './app/templates/**/*.html'
};

gulp.task('js', function(done) {
    var filtered = {
        html: gfilter('**/*.html', { restore: true }),
        js: gfilter('**/*.js', { restore: true })
    };
    pump([
        gulp.src([paths.js, paths.templates]),
        filtered.html,
        templateCache({
            module: 'dreamvillage',
            transformUrl: function (url) {
                console.log(url);
                return 'templates/' + url;
            }
        }),
        filtered.html.restore,
        filtered.js,
        sourcemaps.init(),
        ngAnnotate(),
        uglify(),
        concat('app.min.js'),
        sourcemaps.write('maps'),
        filtered.js.restore,
        gulp.dest('./www/build/js')
    ], done);
});

gulp.task('sass', function(done) {
    gulp.src('./app/scss/ionic.app.scss')
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest('./www/build/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./www/build/css/'))
        .on('end', done);
});

gulp.task('observe', function() {
    gulp.watch([paths.sass], ['sass']);
    gulp.watch([paths.js, paths.templates], ['js']);
});

gulp.task('del', function () {
    del.sync('www/build');
});

gulp.task('deploy', ['del', 'sass', 'js']);

gulp.task('default', ['deploy', 'observe']);

gulp.task('watch', ['deploy', 'observe']);