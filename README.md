# COT Cross platform Ionic frame

## Installeren
### Ubuntu
`curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -;`

`sudo apt-get install -y nodejs;`

`npm i -g ionic cordova;`

### Mac
__Sterk__ aangeraden om Homebrew te gebruiken voor dependencies.

Nog geen Homebrew?

`ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

`brew update && brew doctor`

Alles goed?

```echo 'export PATH="/usr/local/bin:$PATH"' >> ~/.bash_profile; source ~/.bash_profile```

Heb je homebrew al?

`brew install node`


`echo "NPM version: $(npm -v)" && echo "Nodejs version: $(node -v)"`

NPM en Node werkt?

`npm i -g ionic cordova`

### Windows
Nee

___ 

Finally:


`git clone git@bitbucket.org:jordan_salazar/dream-village-frame.git;`

`npm i; bower install;`

`gulp deploy; ionic serve`

## Mappenstructuur


- app                   --- Hier staat alle app specifieke code, je javascript, html en css komen hierin
    - js
        - controllers   --- Alle Angular controllers, meestal 1 per pagina
        - directives    --- Alle directives, denk aan kleine herbruikbare componenten (https://docs.angularjs.org/guide/directive)
        - services      --- Alle services, singleton datamodellen voor de frontend
    - scss              --- Scss is een superset van css met alles wat aan css ontbreekt (http://sass-lang.com/guide)
        - components    --- Herbruikbare css componenten, denk aan een hamburgertje of standaard list styles
        - ionic         --- ionic scss, kun je negeren
        - pages         --- Hier komt alle pagina specifieke scss
    - templates         --- Map voor alle html
        - pages         --- Pagina's
        - partials      --- Componenten
- hooks                 --- Hier komen ionic hooks, informatie in hooks/README 
- www                   --- De webroot van de app
    - build             --- Alle gulp builds, wordt met elke build ge-refreshed
    - img               --- Alle images van de app
    - lib               --- Bower dependencies, alle frontend libraries

## Verder
Met gulp wordt alle JS en SCSS gebundled en naar de build map geschreven. Deze magie staat in gulpfile.js. De templateUrl's van angular kun je altijd nemen met de app/templates als root.

NPM beheert de gulp dependencies (in node_modules) en bower de frontend libraries.

Nieuwe frontend libraries (zoals jquery) kunnen worden toegevoegd met `bower install jquery`

Ionic maakt gebruik van angular componenten, denk aan een checkbox, action sheet of side menu. Hierdoor schrijf je het 1 keer maar hou je platform specifieke styles. Alle informatie hierover staat in de docs:
- http://ionicframework.com/docs

Een beetje inlezen over angular is ook handig, wat een controller doet, hoe directives werken. Angular versie is 1.5.3
- https://code.angularjs.org/1.5.3/docs/api
